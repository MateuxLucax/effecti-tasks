class Sets {
	constructor(private setA: Array<number>, private setB: Array<number>) {}

	joinAndSort(): Array<number> {
		// 1 join both sets
		let setAB: Array<number> = [];
		for (let i = 0; i < this.setA.length; i++) {
			setAB.push(this.setA[i]);
		}

		for (let i = 0; i < this.setB.length; i++) {
			setAB.push(this.setB[i]);
		}

		// 2 Sort the array
		for (let i = 0; i < setAB.length; i++) {
			for (let j = i + 1; j < setAB.length; j++) {
				if (setAB[i] > setAB[j]) {
					let temporary = setAB[i];
					setAB[i] = setAB[j];
					setAB[j] = temporary;
				}
			}
		}

		return setAB;

		// Concat both sets then sort it --> Optimal solution
		// return this.setA.concat(this.setB).sort();
	}
}

const setA = [1, 3, 5, 7];
const setB = [2, 4, 6];

const sumOfSets = new Sets(setA, setB);
console.log(sumOfSets.joinAndSort());
