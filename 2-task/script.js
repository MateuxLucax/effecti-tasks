var Sets = /** @class */ (function () {
	function Sets(setA, setB) {
		this.setA = setA;
		this.setB = setB;
	}
	Sets.prototype.joinAndSort = function () {
		// 1 join both sets
		var setAB = [];
		for (var i = 0; i < this.setA.length; i++) {
			setAB.push(this.setA[i]);
		}
		for (var i = 0; i < this.setB.length; i++) {
			setAB.push(this.setB[i]);
		}
		// 2 Sort the array
		for (var i = 0; i < setAB.length; i++) {
			for (var j = i + 1; j < setAB.length; j++) {
				if (setAB[i] > setAB[j]) {
					var temporary = setAB[i];
					setAB[i] = setAB[j];
					setAB[j] = temporary;
				}
			}
		}
		return setAB;
		// Concat both sets then sort it --> Optimal solution
		// return this.setA.concat(this.setB).sort();
	};
	return Sets;
})();
var setA = [1, 3, 5, 7];
var setB = [2, 4, 6];
var sumOfSets = new Sets(setA, setB);
console.log(sumOfSets.joinAndSort());
