// TODO: implement special character handling
var Dictionary = /** @class */ (function () {
	// Feeds the dictionary with some array of strings
	function Dictionary(dictionary) {
		this.dictionary = dictionary;
	}
	// Implementation similar to (LIKE 'prefix%') on
	Dictionary.prototype.autoComplete = function (prefix) {
		// Filter creates a new array based on a custom validator function
		// https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Array/filtro
		var dictionaryAutocompleted = this.dictionary.filter(function (element) {
			// 1 Checks if the element is equal to prefix
			if (element === prefix) return element;
			// 2 Cycle through each element's character
			for (var index = 0; index < element.length; index++) {
				// 3 splits the element based on index limit, than join it
				var elementPiece = element.split("", index).join("");
				// 4 Checks if element piece is equal to prefix
				if (elementPiece === prefix) return element;
			}
			// Checks if the element's first character is equal to prefix
			// return prefix === element.toLowerCase().charAt(0);
			// Splits the element and check if the first element is equal to prefix
			// return prefix === element.toLowerCase().split("")[0];
			// Checks if the element starts with prefix --> optimal solution
			// return element.startsWith(prefix);
		});
		// .sort();
		// 5 Sort array
		for (var i = 0; i < dictionaryAutocompleted.length; i++) {
			for (var j = i + 1; j < dictionaryAutocompleted.length; j++) {
				if (dictionaryAutocompleted[i] > dictionaryAutocompleted[j]) {
					var temporary = dictionaryAutocompleted[i];
					dictionaryAutocompleted[i] = dictionaryAutocompleted[j];
					dictionaryAutocompleted[j] = temporary;
				}
			}
		}
		return dictionaryAutocompleted;
	};
	return Dictionary;
})();
var demoArray = ["abacate", "alface", "batata", "uva", "a", "aba", "bala"];
var dictionary = new Dictionary(demoArray);
console.log(dictionary.autoComplete("u"));
