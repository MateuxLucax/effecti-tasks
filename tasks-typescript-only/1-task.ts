// TODO: implement special character handling

class Dictionary {
	// Feeds the dictionary with some array of strings
	constructor(private dictionary: Array<string>) {}

	// Implementation similar to (LIKE 'prefix%') on
	autoComplete(prefix: string): Array<string> {
		// Filter creates a new array based on a custom validator function
		// https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Array/filtro
		let dictionaryAutocompleted: Array<string> = this.dictionary.filter(
			(element) => {
				// 1 Checks if the element is equal to prefix
				if (element === prefix) return element;

				// 2 Cycle through each element's character
				for (let index = 0; index < element.length; index++) {
					// 3 splits the element based on index limit, than join it
					let elementPiece: string = element.split("", index).join("");

					// 4 Checks if element piece is equal to prefix
					if (elementPiece === prefix) return element;
				}

				// Checks if the element's first character is equal to prefix
				// return prefix === element.toLowerCase().charAt(0);

				// Splits the element and check if the first element is equal to prefix
				// return prefix === element.toLowerCase().split("")[0];

				// Checks if the element starts with prefix --> optimal solution
				// return element.startsWith(prefix);
			}
		);
		// .sort(); --> Sort array

		// 5 Sort array
		for (let i = 0; i < dictionaryAutocompleted.length; i++) {
			for (let j = i + 1; j < dictionaryAutocompleted.length; j++) {
				if (dictionaryAutocompleted[i] > dictionaryAutocompleted[j]) {
					let temporary = dictionaryAutocompleted[i];
					dictionaryAutocompleted[i] = dictionaryAutocompleted[j];
					dictionaryAutocompleted[j] = temporary;
				}
			}
		}

		return dictionaryAutocompleted;
	}

	// Bonus!!!

	// Implementation similar to (LIKE '%prefix%') on SQL.
	autoCompleteComplex(prefix: string): Array<string> {
		return this.dictionary
			.filter((element) => {
				// 1 Convert prefix and dictionary element to lowercase
				const lowerCasePrefix = prefix.toLowerCase();
				const lowerCaseDictionary = element.toLowerCase();

				// 2 Check if element's matching prefix, via includes() method
				// https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Array/contains
				return lowerCaseDictionary.includes(lowerCasePrefix);
			})
			.sort();
	}
}

const demoArray = ["abacate", "alface", "batata", "uva", "a", "aba", "bala"];
const dictionary = new Dictionary(demoArray);

console.log(dictionary.autoComplete("u"));
