// Object model
interface BookingInterface {
	client: string;
	date: Date;
	table: number;
}

class RestaurantBookings {
	private bookings: Array<BookingInterface>;

	constructor() {
		this.bookings = [];
	}

	// Checks if the table is already booked for that same time
	_validateBookings(date: Date, table: number): boolean {
		// 1 Get all bookings
		const bookings: Array<BookingInterface> = this.bookings;

		// 2 Loop through each booking
		for (let index = 0; index < bookings.length; index++) {
			// 3 Check if booking is already booked for that same time and table
			if (
				// The === == != !== operators requires .getTime() method to compare them
				bookings[index].date.getTime() === date.getTime() &&
				bookings[index].table === table
			)
				return true;
		}
		return false;
	}

	// 1 Add booking
	insert(client: string, date: Date, table: number): Array<Object> {
		// Checks if the date and table are already booked
		if (this._validateBookings(date, table))
			return [{ error: "This table is already booked." }];

		let booking: BookingInterface = { client, date, table };
		this.bookings.push(booking);

		return [this.bookings[this.bookings.length - 1]];
	}

	// 2 Select all bookings
	selectAll(): Array<BookingInterface> {
		return this.bookings;
	}

	// 3 Select specific booking
	selectById(id: number): Array<BookingInterface> {
		if (id === 0) return [this.bookings[0]];

		return this.bookings.filter((booking, index) => {
			if (index === id) return booking;
		});
	}

	// 4 Update booking
	update(id: number, { ...rest }: BookingInterface): Array<Object> {
		// Checks if the date and table are already booked
		if (this._validateBookings(rest.date, rest.table))
			return [{ error: "This table is already booked." }];

		return this.bookings.filter((booking, index) => {
			if (index === id) {
				booking.client = rest.client;
				booking.date = rest.date;
				booking.client = rest.client;
				return booking;
			}
		});
	}

	// 5 Delete booking
	delete(id: number): boolean {
		return !!this.bookings.splice(id, 1);
	}
}

const bookings = new RestaurantBookings();
console.log(bookings.insert("Cooper", new Date("2020-07-23T20:45"), 2));
console.log(bookings.insert("Cooper", new Date("2020-07-23T20:45"), 2));
console.log(bookings.insert("Murphy", new Date("2020-07-23T20:30"), 5));
console.log(bookings.insert("Amelia", new Date("2020-07-22T22:15"), 3));
console.log(
	bookings.insert("Professor Brand", new Date("2020-07-25T21:50"), 4)
);
console.log(bookings.insert("TARS", new Date("2020-07-24T23:45"), 10));
console.log(bookings.selectAll());
console.log(
	bookings.update(2, {
		client: "Tom",
		date: new Date("2020-07-24T23:45"),
		table: 2,
	})
);
console.log(
	bookings.update(1, {
		client: "Tom",
		date: new Date("2020-07-23T20:30"),
		table: 5,
	})
);
console.log(bookings.selectById(2));
console.log(bookings.delete(2));
console.log(bookings.selectById(2));
