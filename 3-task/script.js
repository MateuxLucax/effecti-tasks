var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var RestaurantBookings = /** @class */ (function () {
    function RestaurantBookings() {
        this.bookings = [];
    }
    // Checks if the table is already booked for that same time
    RestaurantBookings.prototype._validateBookings = function (date, table) {
        // 1 Get all bookings
        var bookings = this.bookings;
        // 2 Loop through each booking
        for (var index = 0; index < bookings.length; index++) {
            // 3 Check if booking is already booked for that same time and table
            if (
            // The === == != !== operators requires .getTime() method to compare them
            bookings[index].date.getTime() === date.getTime() &&
                bookings[index].table === table)
                return true;
        }
        return false;
    };
    // 1 Add booking
    RestaurantBookings.prototype.insert = function (client, date, table) {
        // Checks if the date and table are already booked
        if (this._validateBookings(date, table))
            return [{ error: "This table is already booked." }];
        var booking = { client: client, date: date, table: table };
        this.bookings.push(booking);
        return [this.bookings[this.bookings.length - 1]];
    };
    // 2 Select all bookings
    RestaurantBookings.prototype.selectAll = function () {
        return this.bookings;
    };
    // 3 Select specific booking
    RestaurantBookings.prototype.selectById = function (id) {
        if (id === 0)
            return [this.bookings[0]];
        return this.bookings.filter(function (booking, index) {
            if (index === id)
                return booking;
        });
    };
    // 4 Update booking
    RestaurantBookings.prototype.update = function (id, _a) {
        var rest = __rest(_a, []);
        // Checks if the date and table are already booked
        if (this._validateBookings(rest.date, rest.table))
            return [{ error: "This table is already booked." }];
        return this.bookings.filter(function (booking, index) {
            if (index === id) {
                booking.client = rest.client;
                booking.date = rest.date;
                booking.client = rest.client;
                return booking;
            }
        });
    };
    // 5 Delete booking
    RestaurantBookings.prototype["delete"] = function (id) {
        return !!this.bookings.splice(id, 1);
    };
    return RestaurantBookings;
}());
// const bookings = new RestaurantBookings();
// console.log(bookings.insert("Cooper", new Date("2020-07-23T20:45"), 2));
// console.log(bookings.insert("Cooper", new Date("2020-07-23T20:45"), 2));
// console.log(bookings.insert("Murphy", new Date("2020-07-23T20:30"), 5));
// console.log(bookings.insert("Amelia", new Date("2020-07-22T22:15"), 3));
// console.log(
// 	bookings.insert("Professor Brand", new Date("2020-07-25T21:50"), 4)
// );
// console.log(bookings.insert("TARS", new Date("2020-07-24T23:45"), 10));
// console.log(bookings.selectAll());
// console.log(
// 	bookings.update(2, {
// 		client: "Tom",
// 		date: new Date("2020-07-24T23:45"),
// 		table: 2,
// 	})
// );
// console.log(
// 	bookings.update(1, {
// 		client: "Tom",
// 		date: new Date("2020-07-23T20:30"),
// 		table: 5,
// 	})
// );
// console.log(bookings.selectById(2));
// console.log(bookings.delete(2));
// console.log(bookings.selectById(2));
// Terminal application
var bookings = new RestaurantBookings();
var userOption;
alert("Data para cadastro: 2020-07-22T22:15");
while (userOption !== 0) {
    userOption = parseInt(prompt("Escolha uma opção: 1-(Reservar) 2-(Cancelar reserva) 3-(Alterar reserva) 4-(Visualizar reservas) 5-(Visualizar uma reserva especifica) 0-(Encerrar terminal)"));
    // Add booking
    if (userOption === 1) {
        var client = prompt("Digite o nome do cliente");
        var date = new Date(prompt("Digite a data"));
        var table = parseInt(prompt("Digite a mesa"));
        console.log("Reserva:");
        console.log(bookings.insert(client, date, table)[0]);
    }
    // Delete booking
    else if (userOption === 2) {
        var id = parseInt(prompt("Digite o id da reserva"));
        console.log(bookings["delete"](id));
    }
    // Update booking
    else if (userOption === 3) {
        var id = parseInt(prompt("Digite o id da reserva que deseja alterar"));
        var client = prompt("Digite o nome do cliente");
        var date = new Date(prompt("Digite a data"));
        var table = parseInt(prompt("Digite a mesa"));
        console.log("Reserva:");
        console.log(bookings.update(id, {
            client: client,
            date: date,
            table: table
        })[0]);
    }
    // Select all bookings
    else if (userOption === 4) {
        console.log("Reservas:");
        bookings.selectAll().forEach(function (booking) { return console.log(booking); });
    }
    // Select booking
    else if (userOption === 5) {
        console.log("Reserva:");
        var id = parseInt(prompt("Digite o id da reserva"));
        console.log(bookings.selectById(id)[0]);
    }
}
